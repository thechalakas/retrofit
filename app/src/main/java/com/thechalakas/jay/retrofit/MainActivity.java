package com.thechalakas.jay.retrofit;

import android.database.Observable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class MainActivity extends AppCompatActivity
{

    //Creating the Retrofit instance
    // Trailing slash is needed
    public static final String BASE_URL = "http://simplewebapi1webapp1.azurewebsites.net/";
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //use the interface and create an endpoint
        MyApiEndpointInterface myApiEndpointInterface = retrofit.create(MyApiEndpointInterface.class);
        //list of headphones

        Call<List<HeadPhone>> call = myApiEndpointInterface.headphones();

        call.enqueue(new Callback<List<HeadPhone>>()
        {
            @Override
            public void onResponse(Call<List<HeadPhone>> call, Response<List<HeadPhone>> response)
            {
                Log.i("MainActivity","call.enqueue - onResponse");
                //Log.i("MainActivity","Response details - " + response.toString());
                List<HeadPhone> list = response.body();
                Log.i("MainActivity","data details - " + list.toArray().toString());

                for (int i=0; i<list.size(); i++)
                {
                    //System.out.println(list.get(i));
                    if(list.get(i).getName() != null)
                    {
                        Log.i("MainActivity","data details - " + list.get(i).getName().toString());
                    }
                }

            }

            @Override
            public void onFailure(Call<List<HeadPhone>> call, Throwable t)
            {
                Log.i("MainActivity","call.enqueue - onFailure");
            }
        });

        //add a headphone
        HeadPhoneSend headPhoneSend = new HeadPhoneSend();
        Warehouse warehouse = new Warehouse();
        //first set the warehouse
        warehouse.setId(50);warehouse.setWarehouseName("android warehouse");
        headPhoneSend.setId(101);
        headPhoneSend.setCategory("android category");
        headPhoneSend.setName("android headphones");
        headPhoneSend.setPrice(100.69);
        headPhoneSend.setWarehouse(warehouse);
        headPhoneSend.setWarehouseId(50);

        //Observable<HeadPhoneSend> observable = myApiEndpointInterface.addheadphone(headPhoneSend);
        Call<HeadPhoneSend> call1 = myApiEndpointInterface.addheadphone(headPhoneSend);

        call1.enqueue(new Callback<HeadPhoneSend>()
        {
            @Override
            public void onResponse(Call<HeadPhoneSend> call, Response<HeadPhoneSend> response)
            {
                Log.i("MainActivity","call1.enqueue - Success - Headphone has been added!");

            }

            @Override
            public void onFailure(Call<HeadPhoneSend> call, Throwable t)
            {
                Log.i("MainActivity","call1.enqueue - onFailure");
            }
        });

    }

    public interface MyApiEndpointInterface
    {

        @GET("api/Headphones")
        Call<List<HeadPhone>> headphones();

        //@POST("api/Headphones")
        //Observable<HeadPhoneSend> addheadphone(@Body HeadPhoneSend headPhoneSend);

        @POST("api/Headphones")
        Call<HeadPhoneSend> addheadphone(@Body HeadPhoneSend headPhoneSend);
    }
}
